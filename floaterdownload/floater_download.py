#!/usr/bin/python3

import  sys
import  os
import  requests
import  webbrowser
import  csv
from    bs4 import BeautifulSoup
from    sys import platform

__version__ = "0.2.0"

### Defines ###
headers         = {                                     #Headers for http requests
                'User-Agent': 'UVM Tech Team',
                'From'      : 'helpline@uvm.edu'
                }

programs                = []
max_diag_chars          = 0                           #Maximum chars printed per step of search
log_toggle              = False                         #Set to false to disable logging
print_toggle            = True                          #Set to false to disable printing
verbose_toggle          = False

directory               = os.getcwd()                   #Current working directory    

config_file_location    = './config'                #Location of config file
log_file_location       = './log'                   #Location of log file
out_folder_location     = directory

log_file                = None                          #Initialize log file
args                    = sys.argv                      #Wrapper for arg

### Start Program ###
def main():
    global log_file_location, out_folder_location, config_file_location, log_toggle, log_file       #Make sure vars are global.  There is likely a better way to do this.
    global max_diag_chars, verbose_toggle
    log_file = open(log_file_location, 'w')                                                         #Opening the log file regardless of whether logging is enabled.  We'll just delete it later if not needed.

    for x in args[1:]:                                                                              #Skip the first argument as this is just the name of the program.
        if x.startswith('log='):                                                                    #Specify log file location.
            log_file_location = os.path.join(directory,x[4:])               
            log_toggle = True
            log_file = open(log_file_location, 'w')
        elif x.startswith('config='):                                                               #Specify config file location.
            config_file_location = os.path.join(directory,x[7:])
        elif x.startswith('out='):                                                                  #Specify output directory.
            out_folder_location = os.path.join(directory,x[4:])
        elif x.startswith('chars='):                                                                #Specify maximum number of characters to be printed per step of search.
            max_diag_chars = int(x[6:])
        elif x[0] == '-':
            for char in x:
                if char == 'l':                                                                     #Enable Logging.
                    log_toggle = True
                    log('Logging Enabled', True)
                if char == 'q':                                                                     #Enable Quiet Mode.
                    print_toggle = False
                    log('Quiet Mode', True)
                if char == 'v':                                                                     #Enable Verbose Mode.
                    log('Verbose Mode', True)
                    verbose_toggle = True

    if not os.path.exists(out_folder_location):                                                     #Make sure output directory exists.
        os.makedirs(out_folder_location)

    if log_toggle:
        log('Using log file at: ' + log_file_location, True)
    
    log('Using config file at: ' + config_file_location, True)
    log('Writing to folder at: ' + out_folder_location, True)
    
    if max_diag_chars > 0:
        log('Printing up to ' + str(max_diag_chars) + ' characters per search step.')

    try:
        with open(config_file_location) as csv_file:                                                    
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if row[0][0]=='#':                                                   
                   line_count+=1
                else:
                   programs.append(row)
    except:
        log("Unable to find config file at: " + config_file_location, True)
        sys.exit()


    log('\n\n#Downloading the following files: ', True)
    for program in programs:
        log(program[0] + '.' + program[3] + ' --- (' + program[1] + ')', True)
    log('\n\n', True)

    for program in programs:
        download(program)
    
    log('Operation Complete\n', True)
    
    if log_toggle:
        log('See log file at ' + log_file_location + 'for more information.\n')
      
    log_file.close()
    if not log_toggle:    
        os.remove(log_file_location)


#Prints a message both to the screen and to the log file.  Both outputs can be toggled.
def log(message, important=False):
    global log_toggle, log_file
    if important or verbose_toggle:
        if print_toggle:
            print(message)
    if log_toggle:
        log_file.write(message+'\n')
    
#Shamelessly stolen from SO, just finds the nth instance of needle in string haystack.
def find_nth(haystack, needle, n):
    start = haystack.find(needle)
    while start >= 0 and n > 1:
        start = haystack.find(needle, start+len(needle))
        n -= 1
    return start

#Given the link locator, page code, and url, finds the link to download the program.
def get_download_link(soup, link_location, download_url):           
    nav_path = link_location.split(':')                         
    location = soup
    iterations = 0
    for node in nav_path:   
        if iterations > 0:                                      #First Node
            log('\n#Current Scope:')
            if not max_diag_chars == 0:
                log(str(location)[:max_diag_chars]+'\n')
            else:
                log(str(location)+'\n')
        if node[0] == '.':                                      #Class
            location = location.find(class_=node[1:])
            log('#Finding class "' + node[1:] + '"...')
        elif node[0] == '#':                                    #ID
            location = location.find(id=node[1:])
            log('#Finding id "' + node[1:] + '"...')
        elif node == '^':                                       #Go to parent
            location = location.parent
            log('#Moving to parent...')
        elif node[0] == '*':                                    #Get attribute
            log('#Getting href...')
            location = location.get_attribute_list('href')[0]
        elif node[0] == '@':                                    #Find title
            location = location.find(title=node[1:])
        else:                                                   #Tag name
            log('#Finding "' + node + '" tag...')
            location = location.find(name=node)
        iterations += 1
    
    log('#Current Location:')
    log(location)
    
    if not location.startswith('http'):                                         #If download link is relative,
        location = download_url[:find_nth(download_url, '/', 3)] + location     # append it to original link
        log('#Relative URL found')
        log('#Edited location: ' + location)

    return location

#Given program details from config file, finds and tries to download program
def download(program_details):
    program_name        = program_details[0] + '.' + program_details[3]     #Name and file type of Program
    download_url        = program_details[1]                                #URL of Download Page
    link_location       = program_details[2]                                #Locator string for download link
    operating_system    = program_details[4]                                #Target OS
    
    log('##########  '+program_name+'  ##########', True)
    log('Searching for download at URL: '+download_url, True)

    if link_location == '+':                                                
        log('#Downloading directly at url: '+download_url, True)
        download_link = download_url
    elif link_location == '-':                                              
        log('#Manual Download Required. \nOpening url: '+download_url, True)
        webbrowser.open(download_url, 2, True)
        download_link = False
    else:                                                                    
        web_page = requests.get(download_url, headers = headers)
        soup = BeautifulSoup(web_page.text, 'html.parser')
        
        #If download fails, open page in browser
        try:                                                                
            download_link = get_download_link(soup, link_location, download_url)
        except:
            download_link = False
            log('Error: Automatic Download Failed', True)
            log('Opening site in browser; please correct config file.', True)

            if log_toggle:
                log('See log at '+log_file_location+' for more information.', True)

            webbrowser.open(download_url, 2, True)

    if not download_link == False:                                          #Download File                        
        download_file = requests.get(download_link, headers = headers, allow_redirects=True)                                
   
        if not os.path.exists(os.path.join(out_folder_location,operating_system)):                            #Save in appropriate folder    
            os.makedirs(os.path.join(out_folder_location,operating_system))
        open(os.path.join(out_folder_location,operating_system,program_name), 'wb').write(download_file.content)
        log('#'+program_name + ' downloaded.', True)
        log('#Saved to '+ os.path.join(out_folder_location,operating_system,program_name), True)
        log('\n\n', True)

if __name__=="__main__":
    main()
