import setuptools, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="floaterdownload",
    version="0.1.0",
    author="Willem Hunt",
    author_email="whunt1@uvm.edu",
    description="A program to automatically download free software from the web",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.uvm.edu/whunt1/floaterdownload",
    packages=find_packages('docs','tests'),
    install_requires=['requests','bs4', 'sys', 'os', 'webbrowser', 'csv'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: MIT License"
    ],
    package_dir={"floaterdownload": "floaterdownload"},
    package_data={"floaterdownload": "*.csv"},
    entry_points={
        'console_scripts': [
            'floaterdownload=floaterdownload:main',
        ],
    }
)
